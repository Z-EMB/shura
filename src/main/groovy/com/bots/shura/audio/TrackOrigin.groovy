package com.bots.shura.audio

enum TrackOrigin {
    SINGLE,
    PLAYLIST
}